import {Client} from '../classes/client';
import {Activity} from '../classes/activity';

export interface ActivityInterface {
  client: Client;
  activity: Activity;
}
