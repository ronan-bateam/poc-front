import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  objectKeys = Object.keys;
  maxProf: number;
  elements = [
    {
      id : 2,
      inf : 2,
      sup : 3,
      type: 'ldp',
      data : [
        {id: 'num', data: '1.1'},
        {id: 'libelle', data: 'CHAUFFAGE ET CLIMATISATION'},
        {id: 'commentaire', data: 'Réseau de chauffage horizontal en sous-sol ou galerie technique réalisé en tube fer noir D 40 x 49 mm.'},
        {id: 'fournisseur', data: 'Nordy'},
        {id: 'qtu', data: '2'},
        {id: 'qtot', data: '2'},
        {id: 'pa', data: '15.3'},
        {id: 'puht', data: '20'},
        {id: 'totalht', data: '40'},
        {id: 'totalht', data: '1'},
      ],
    },
    {
      id : 1,
      inf : 1,
      sup : 6,
      type: 'titre',
      data : [
        {id: 'num', data: '1'},
        {id: 'libelle', data: 'CHAUFFAGE ET CLIMATISATION'},
        {id: 'commentaire', data: 'Réseau de chauffage horizontal en sous-sol ou galerie technique réalisé en tube fer noir D 40 x 49 mm.'},
        {id: 'fournisseur', data: 'Nordy'},
        {id: 'qtu', data: '2'},
        {id: 'qtot', data: '2'},
        {id: 'pa', data: '15.3'},
        {id: 'puht', data: '20'},
        {id: 'totalht', data: '40'},
        {id: 'totalht', data: '1'},
      ],
    },
    {
      id : 3,
      inf : 4,
      sup : 5,
      type: 'ldp',
      data : [
        {id: 'num', data: '1.2'},
        {id: 'libelle', data: 'CHAUFFAGE ET CLIMATISATION'},
        {id: 'commentaire', data: 'Réseau de chauffage horizontal en sous-sol ou galerie technique réalisé en tube fer noir D 40 x 49 mm.'},
        {id: 'fournisseur', data: 'Nordy'},
        {id: 'qtu', data: '2'},
        {id: 'qtot', data: '2'},
        {id: 'pa', data: '15.3'},
        {id: 'puht', data: '20'},
        {id: 'totalht', data: '40'},
        {id: 'totalht', data: '1'},
      ],
    },
    {
      id : 4,
      inf : 7,
      sup : 14,
      type: 'titre',
      data : [
        {id: 'num', data: '2'},
        {id: 'libelle', data: 'CHAUFFAGE ET CLIMATISATION'},
        {id: 'commentaire', data: 'Réseau de chauffage horizontal en sous-sol ou galerie technique réalisé en tube fer noir D 40 x 49 mm.'},
        {id: 'fournisseur', data: 'Nordy'},
        {id: 'qtu', data: '2'},
        {id: 'qtot', data: '2'},
        {id: 'pa', data: '15.3'},
        {id: 'puht', data: '20'},
        {id: 'totalht', data: '40'},
        {id: 'totalht', data: '1'},
      ],
    },
    {
      id : 5,
      inf : 8,
      sup : 11,
      type: 'ldp',
      data : [
        {id: 'num', data: '2.1'},
        {id: 'libelle', data: 'CHAUFFAGE ET CLIMATISATION'},
        {id: 'commentaire', data: 'Réseau de chauffage horizontal en sous-sol ou galerie technique réalisé en tube fer noir D 40 x 49 mm.'},
        {id: 'fournisseur', data: 'Nordy'},
        {id: 'qtu', data: '2'},
        {id: 'qtot', data: '2'},
        {id: 'pa', data: '15.3'},
        {id: 'puht', data: '20'},
        {id: 'totalht', data: '40'},
        {id: 'totalht', data: '1'},
      ],
    },
    {
      id : 6,
      inf : 12,
      sup : 13,
      type: 'ldp',
      data : [
        {id: 'num', data: '2.2'},
        {id: 'libelle', data: 'CHAUFFAGE ET CLIMATISATION'},
        {id: 'commentaire', data: 'Réseau de chauffage horizontal en sous-sol ou galerie technique réalisé en tube fer noir D 40 x 49 mm.'},
        {id: 'fournisseur', data: 'Nordy'},
        {id: 'qtu', data: '2'},
        {id: 'qtot', data: '2'},
        {id: 'pa', data: '15.3'},
        {id: 'puht', data: '20'},
        {id: 'totalht', data: '40'},
        {id: 'totalht', data: '1'},
      ],
    },
    {
      id : 7,
      inf : 9,
      sup : 10,
      type: 'article',
      data : [
        {id: 'num', data: '2.1.1'},
        {id: 'libelle', data: 'CHAUFFAGE ET CLIMATISATION'},
        {id: 'commentaire', data: 'Réseau de chauffage horizontal en sous-sol ou galerie technique réalisé en tube fer noir D 40 x 49 mm.'},
        {id: 'fournisseur', data: 'Nordy'},
        {id: 'qtu', data: '2'},
        {id: 'qtot', data: '2'},
        {id: 'pa', data: '15.3'},
        {id: 'puht', data: '20'},
        {id: 'totalht', data: '40'},
        {id: 'totalht', data: '1'},
      ],
    },
  ];

  columns = [
    {order: 0, libelle : '', width: 4},
    {order: 1, libelle : 'Num', width: 10},
    {order: 2, libelle : 'Libelle', width: 10},
    {order: 2, libelle : 'Commentaire', width: 10},
    {order: 3, libelle : 'Fournisseur', width: 10},
    {order: 4, libelle : 'Qté unitaire', width: 5},
    {order: 5, libelle : 'Qté totale', width: 5},
    {order: 6, libelle : 'Déboursé sec', width: 10},
    {order: 7, libelle : 'Prix unitaire HT', width: 10},
    {order: 8, libelle : 'Total HT', width: 10},
    {order: 9, libelle : 'Durée', width: 10},
  ];

  constructor() {
    this.maxProf = this.getNiveauMax();
  }

  ngOnInit(): void {
    this.sortElement();
  }

  updateAllParentsValue() {
    for (let i in this.elements) {
      let parent = this.getParent(this.elements[i]);
      let newVal = parent ? this.getValue(parent, 'num') : 0;
      this.updateValue(this.elements[i], 'parent', newVal);
    }
  }

  updateValue(element, typeValue, newValue) {
    for (let i in element.data) {
      if (element.data[i].id == typeValue) {
        element.data[i].data = newValue;
      }
    }
  }

  getValue(element, typeValue) {
    for (let i in element.data) {
      if (element.data[i].id == typeValue) {
        return element.data[i].data;
      }
    }
  }

  getBorneMax() {
    let borneMax = 0;
    for (let i in this.elements) {
      if (this.elements[i].sup > borneMax) {
        borneMax = this.elements[i].sup;
      }
    }

    return borneMax;
  }

  getDeltaTable(element) {
    return this.getBorneMax() + 1 - element.inf;
  }

  sortElement() {
    this.elements.sort((a, b) => (a.inf > b.inf) ? 1 : -1);
  }

  isChildOf(parent, element) {
    if (!parent) {
      return false;
    } else {
      return element.inf > parent.inf && element.sup < parent.sup;
    }
  }

  isLeaf(element) {
    return (this.getInterval(element) === 1);
  }

  getAllChilds(parent) {
    let childs = [];
    for (let i in this.elements) {
      if (this.elements[i].inf > parent.inf && this.elements[i].sup < parent.sup) {
        childs.push(i);
      }
    }
    return childs;
  }

  getInterval(element) {
    return element.sup - element.inf;
  }

  getParents(element) {
    let parents = [];
    for (let i in this.elements) {
      if (this.elements[i].sup > element.sup && this.elements[i].inf < element.inf) {
        parents.push(this.elements[i]);
      }
    }

    return parents;
  }

  getParent(element) {
    let parent = null;
    let borneInf = 0;
    for (let i in this.elements) {
      if (this.elements[i].inf < element.inf && this.elements[i].inf > borneInf && !this.isLeaf(this.elements[i]) && this.isChildOf(this.elements[i], element)) {
        borneInf = this.elements[i].inf;
        parent = this.elements[i];
      }
    }

    return parent;
  }

  getNiveauMax() {
    let niveauMax = 0;
    let niveau = 0;
    for (let i in this.elements) {
      niveau = this.getParents(this.elements[i]).length;
      if(niveau > niveauMax) {
        niveauMax = niveau;
      }
    }

    return niveauMax;
  }

  /* Fonctions des noeuds */
  moveNodeAtEndOfList(element, delta, borneMax) {
    const deltaChild = element.sup - element.inf + 1;
    const eltInf = element.inf;
    const eltSup = element.sup;

    for (let i in this.elements) {
      if (this.elements[i].inf >= eltInf && this.elements[i].sup <= eltSup) {
        this.elements[i].inf = this.elements[i].inf + delta;
        this.elements[i].sup = this.elements[i].sup + delta;
      }
      if (this.elements[i].sup > eltSup && this.elements[i].inf <= borneMax) {
        this.elements[i].sup = this.elements[i].sup - deltaChild;
      }
      if (this.elements[i].inf > eltSup && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf - deltaChild;
      }
    }
  }

  moveNodeAsFistChild(parent, element) {
    const delta = this.getDeltaTable(element);
    const borneMax = this.getBorneMax();
    const deltaChild = borneMax - parent.inf;

    this.moveNodeAtEndOfList(element, delta, borneMax);

    for (let i in this.elements) {
      if (this.elements[i].sup > parent.inf && this.elements[i].sup <= borneMax) {
        this.elements[i].sup = this.elements[i].sup + delta;
      }
      if (this.elements[i].inf > parent.inf && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf + delta;
      }
      if (this.elements[i].inf > borneMax) {
        this.elements[i].inf = this.elements[i].inf - deltaChild;
        this.elements[i].sup = this.elements[i].sup - deltaChild;
      }
    }
  }

  moveNodeAsLastChild(parent, element) {
    const delta = this.getDeltaTable(element);
    const borneMax = this.getBorneMax();
    const deltaChild = borneMax - parent.sup + 1;

    this.moveNodeAtEndOfList(element, delta, borneMax);

    for (let i in this.elements) {
      if (this.elements[i].sup >= parent.inf && this.elements[i].sup <= borneMax) {
        this.elements[i].sup = this.elements[i].sup + delta;
      }
      if (this.elements[i].inf > parent.sup && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf + delta;
      }
      if (this.elements[i].inf > borneMax) {
        this.elements[i].inf = this.elements[i].inf - deltaChild;
        this.elements[i].sup = this.elements[i].sup - deltaChild;
      }
    }
  }

  moveNodeAsLilBrother(brother, element) {
    const delta = this.getDeltaTable(element);
    const borneMax = this.getBorneMax();
    const deltaChild = borneMax - brother.sup;

    this.moveNodeAtEndOfList(element, delta, borneMax);

    for (let i in this.elements) {
      if (this.elements[i].sup > brother.sup && this.elements[i].sup <= borneMax) {
        this.elements[i].sup = this.elements[i].sup + delta;
      }
      if (this.elements[i].inf > brother.sup && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf + delta;
      }
      if (this.elements[i].inf > borneMax) {
        this.elements[i].inf = this.elements[i].inf - deltaChild;
        this.elements[i].sup = this.elements[i].sup - deltaChild;
      }
    }
  }

  moveNodeAsBigBrother(brother, element) {
    const delta = this.getDeltaTable(element);
    const borneMax = this.getBorneMax();
    const deltaChild = borneMax - brother.inf + 1;

    this.moveNodeAtEndOfList(element, delta, borneMax);

    for (let i in this.elements) {
      if (this.elements[i].sup > brother.inf && this.elements[i].sup <= borneMax) {
        this.elements[i].sup = this.elements[i].sup + delta;
      }
      if (this.elements[i].inf >= brother.inf && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf + delta;
      }
      if (this.elements[i].inf > borneMax) {
        this.elements[i].inf = this.elements[i].inf - deltaChild;
        this.elements[i].sup = this.elements[i].sup - deltaChild;
      }
    }
  }

  /* Fonctions des Feuilles */
  moveLeafAtEndOfList(element, delta, borneMax) {
    const eltSup = element.sup;
    element.inf = element.inf + delta;
    element.sup = element.sup + delta;
    for (let i in this.elements) {
      if (this.elements[i].inf > eltSup && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf - 2;
      }
      if (this.elements[i].sup > eltSup && this.elements[i].sup <= borneMax) {
        this.elements[i].sup = this.elements[i].sup - 2;
      }
    }
  }

  moveLeafAsFistChild(parent, element) {
    const delta = this.getDeltaTable(element);
    const borneMax = this.getBorneMax();
    const parentInf = parent.inf;

    this.moveLeafAtEndOfList(element, delta, borneMax);

    for (let i in this.elements) {
      if (this.elements[i].sup > parentInf && this.elements[i].sup <= borneMax) {
        this.elements[i].sup = this.elements[i].sup + 2;
      }
      if (this.elements[i].inf > parentInf && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf + 2;
      }
    }
    element.inf = parentInf + 1;
    element.sup = parentInf + 2;
  }

  moveLeafAsLilBrother(parent, element) {
    const delta = this.getDeltaTable(element);
    const borneMax = this.getBorneMax();
    const parentSup = parent.sup;

    this.moveLeafAtEndOfList(element, delta, borneMax)
    ;
    for (let i in this.elements) {
      if (this.elements[i].sup > parentSup && this.elements[i].sup <= borneMax) {
        this.elements[i].sup = this.elements[i].sup + 2;
      }
      if (this.elements[i].inf > parentSup && this.elements[i].inf < borneMax) {
        this.elements[i].inf = this.elements[i].inf + 2;
      }
      element.inf = parentSup + 1;
      element.sup = parentSup + 2;
    }
  }

  /* Evènements */

  dropElement(event: CdkDragDrop<string[]>) {
    if (this.isLeaf(this.elements[event.currentIndex])) {
      // Si on est sur une feuille
      if (event.currentIndex === 0) {

      } else if (this.isChildOf(this.elements[event.currentIndex - 1], this.elements[event.currentIndex])) {
        // Si l'élément du dessus est le parent de l'element du dessous on met la feuille sous le parent
        this.moveLeafAsFistChild(this.elements[event.currentIndex - 1], this.elements[event.previousIndex]);
      } else {
        // Sinon on met la feuille en tant que petit frère de l'élement supérieur
        this.moveLeafAsLilBrother(this.elements[event.currentIndex - 1], this.elements[event.previousIndex]);
      }
    } else {
      // si on est sur un noeud
      if (event.currentIndex === 0) {
        this.moveNodeAsBigBrother(this.elements[event.currentIndex + 1], this.elements[event.previousIndex]);
      } else if (this.isChildOf(this.elements[event.currentIndex - 1], this.elements[event.currentIndex])) {
        // Si l'élément du dessus est le parent de l'element du dessous on met le noeud sous le parent
        this.moveNodeAsFistChild(this.elements[event.currentIndex - 1], this.elements[event.previousIndex]);
      } else {
        // Sinon on met le noeud en tant que petit frère de l'élement supérieur
        this.moveNodeAsLilBrother(this.elements[event.currentIndex - 1], this.elements[event.previousIndex]);
      }
    }

    //this.updateAllParentsValue();
    this.sortElement();
  }

  dropColumn(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columns, event.previousIndex, event.currentIndex);
    for (let i = 0; i <= this.elements.length; i++) {
      moveItemInArray(this.elements[i].data, event.previousIndex, event.currentIndex);
    }
  }
}
