import {Employes} from './employes';
import {Client} from './client';
import {Activity} from './activity';
import {Adresse} from './adresse';

export class Devis {
  private id: number;
  private libelle: string;
  private objet: string;
  private dateDebut: Date;
  private numCdeClient: string;
  private suiviPar = new Employes();
  private donneurOrdre: Employes[];
  private proprietaire: Employes[];
  private client: Client;
  private activity: Activity;
  private adresseFacturation: Adresse;
  private adresseLivraison: Adresse;

  constructor() {}

  getId(): number {
    return this.id;
  }

  setId(value: number) {
    this.id = value;
  }

  getLibelle(): string {
    return this.libelle;
  }

  setLibelle(value: string) {
    this.libelle = value;
  }

  getObjet(): string {
    return this.objet;
  }

  setObjet(value: string) {
    this.objet = value;
  }

  getDateDebut(): Date {
    return this.dateDebut;
  }

  setDateDebut(value: Date) {
    this.dateDebut = value;
  }

  getNumCdeClient(): string {
    return this.numCdeClient;
  }

  setNumCdeClient(value: string) {
    this.numCdeClient = value;
  }

  getSuiviPar(): Employes {
    return this.suiviPar;
  }

  setSuiviPar(value: Employes) {
    this.suiviPar = value;
  }

  getDonneurOrdre(): Employes[] {
    return this.donneurOrdre;
  }

  setDonneurOrdre(value: Employes[]) {
    this.donneurOrdre = value;
  }

  getProprietaire(): Employes[] {
    return this.proprietaire;
  }

  setProprietaire(value: Employes[]) {
    this.proprietaire = value;
  }

  getClient(): Client {
    return this.client;
  }

  setClient(value: Client) {
    this.client = value;
  }

  getActivity(): Activity {
    return this.activity;
  }

  setActivity(value: Activity) {
    this.activity = value;
  }

  getAdresseFacturation(): Adresse {
    return this.adresseFacturation;
  }

  setAdresseFacturation(value: Adresse) {
    this.adresseFacturation = value;
  }

  getAdresseLivraison(): Adresse {
    return this.adresseLivraison;
  }

  setAdresseLivraison(value: Adresse) {
    this.adresseLivraison = value;
  }

  getDefinition() {
    return [
      {key: 'libelle', libelle: 'Libelle', type: 'text'},
      {key: 'objet', libelle: 'Objet', type: 'text'},
      {key: 'suiviPar', libelle: 'Suivi par', type: 'object'},
      {key: 'donneurOrdre', libelle: 'Donneur d\'ordre', type: 'array'},
      {key: 'proprietaire', libelle: 'Propriétaire', type: 'array'},
      {key: 'client', libelle: 'Client', type: 'object', modifiable: true, creatable: true},
      {key: 'dateDebut', libelle: 'Date début', type: 'date'},
      {key: 'activity', libelle: 'Activité', type: 'object', modifiable: true, creatable: true},
      {key: 'adresseFacturation', libelle: 'Adresse de facturation', type: 'object', modifiable: true},
      {key: 'adresseLivraison', libelle: 'Adrresse de livraison', type: 'object', modifiable: true},
      {key: 'numCdeClient', libelle: 'Numéro de commande client', type: 'text'},
    ];
  }

  getImpression() {
    return [
      {key: 'libelle', libelle: 'Libelle', type: 'text'},
      {key: 'objet', libelle: 'Objet', type: 'text'},
      {key: 'suiviPar', libelle: 'Suivi par', type: 'object'},
      {key: 'donneurOrdre', libelle: 'Donneur d\'ordre', type: 'array'},
      {key: 'proprietaire', libelle: 'Propriétaire', type: 'array'},
      {key: 'client', libelle: 'Client', type: 'object', modifiable: true, creatable: true},
      {key: 'dateDebut', libelle: 'Date début', type: 'date'},
      {key: 'activity', libelle: 'Activité', type: 'object', modifiable: true, creatable: true},
      {key: 'adresseFacturation', libelle: 'Adresse de facturation', type: 'object', modifiable: true},
      {key: 'adresseLivraison', libelle: 'Adrresse de livraison', type: 'object', modifiable: true},
      {key: 'numCdeClient', libelle: 'Numéro de commande client', type: 'text'},
    ];
  }
}
