import { Component, OnInit } from '@angular/core';
import {Devis} from '../../classes/devis';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-devis-form',
  templateUrl: './devis-form.component.html',
  styleUrls: ['./devis-form.component.css']
})
export class DevisFormComponent implements OnInit {
  devis: Devis;
  devisForm: FormGroup;
  definitionDevis: object[];

  employes = [
    {id: 1, name: 'Marc Eymard'},
    {id: 2, name: 'Jonathan Morel'},
    {id: 3, name: 'Ronan Lombard'},
    {id: 4, name: 'Jeremy Georges'},
    {id: 5, name: 'Kevin Ohayon'},
    {id: 6, name: 'Matthieu Gogniat'},
  ];
  chantiers = [
    {id: 1, name: 'Maçonnerie maison'},
    {id: 2, name: 'Réfaction de toiture'},
    {id: 3, name: 'Pose de placard'},
    {id: 4, name: 'Peinture du chien'},
    {id: 5, name: 'Frisage de moustache de chat'},
    {id: 6, name: 'Epluchage des patates pour faire une bonne purée'},
  ];
  adresses = [
    {id: 1, name: '23 rue de la petite bergère, 59220 Cysoing'},
    {id: 2, name: '19 rue Howard Philippe, 45000 Autruie sur Jouine'},
    {id: 3, name: '89 avenue du petit père, 26000 Valence'},
  ];

  constructor() {
    this.devis = new Devis();
    this.devisForm = this.createFormGroup();
    this.definitionDevis = this.devis.getDefinition();
  }

  ngOnInit(): void {}

  createFormGroup() {
    return new FormGroup({
      libelle: new FormControl(),
      objet: new FormControl(),
      dateDebut: new FormControl(),
      numCdeClient: new FormControl(),
      suiviPar: new FormControl(),
      donneurOrdre: new FormControl(),
      proprietaire: new FormControl(),
      client: new FormControl(),
      activity: new FormControl(),
      adresseFacturation: new FormControl(),
      adresseLivraison:new FormControl()
    });
  }
}
